//
//  ArticleTableViewCell.swift
//  swifty-demo
//
//  Created by Mavin on 10/11/21.
//

import UIKit
import Kingfisher
import Foundation

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    
    @IBOutlet weak var authorLabel: UILabel!
    
    let author: [String] = ["Leangseng", "Mavin", "Kimleang"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func config(article: Article){
        self.titleLabel.text = article.title
        self.descriptionLabel.text = article.description
        self.dateLabel.text = article.createdAt.ReadableDate()
        
        let url = URL(string: article.imageUrl)
        
        let defaultImage = UIImage(named: "no-image")
        
        self.articleImageView.kf.setImage(with: url,placeholder: defaultImage, options: [.transition(.fade(0.25))])
        
        self.authorLabel.text = author[Int.random(in: 0...2)]
        
    }

   

}
