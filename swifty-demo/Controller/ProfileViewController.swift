//
//  ProfileViewController.swift
//  swifty-demo
//
//  Created by Kheang on 26/11/21.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var frameView: UIView!
    
    @IBOutlet weak var langSwitch: UISwitch!
    
    @IBOutlet weak var languageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func changeLang(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name("changeLang"), object: nil, userInfo: ["lang": langSwitch.isOn])
        
        if langSwitch.isOn{
            AppService.shared.changeLang(lang: "en")
            self.title = "Profile".localizedString()
            self.languageLabel.text = "Language".localizedString()
       }else{
           AppService.shared.changeLang(lang: "km")
           self.title = "Profile".localizedString()
           self.languageLabel.text = "Language".localizedString()
       }
        
    }
    @IBAction func button1(_ sender: Any) {
        frameView.backgroundColor = UIColor.orange
    }
    
    @IBAction func button2(_ sender: Any) {
        frameView.backgroundColor = UIColor.systemPink
    }
    
    @IBAction func button3(_ sender: Any) {
        frameView.backgroundColor = UIColor.systemIndigo
    }
    @IBAction func button4(_ sender: Any) {
        frameView.backgroundColor = UIColor.tintColor
    }
}
