//
//  DetailtViewController.swift
//  swifty-demo
//
//  Created by Mavin on 10/11/21.
//

import UIKit
import Foundation

class DetailtViewController: UIViewController {

    var article: Article?
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: article!.imageUrl)
        
        let defaultImage = UIImage(systemName: "camera.fill")
                
        self.imageView.kf.setImage(with: url,placeholder: defaultImage, options: [.transition(.fade(0.25))])
        
        titleLabel.text = article?.title
        textView.text = article?.description
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
