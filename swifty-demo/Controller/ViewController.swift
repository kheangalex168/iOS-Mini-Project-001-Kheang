//
//  ViewController.swift
//  swifty-demo
//
//  Created by Mavin on 10/11/21.
//

import UIKit
import ProgressHUD

class ViewController: UIViewController {
    
    @IBOutlet weak var tableVIew: UITableView!
    
    private let searchController = UISearchController()
    
    private var filteredArticle: [Article] = []
    
    var articles: [Article] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // initializing the refreshControl
        tableVIew.refreshControl = UIRefreshControl()
        
        // add target to UIRefreshControl
        tableVIew.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        fetch()
        configureSearchBar()
        subcribe()
     
    }
    
    @objc func refresh(){
        fetch()
    }
    
    func configureSearchBar() {
        navigationItem.searchController = searchController
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.delegate = self
    }
    
    func subcribe(){
        NotificationCenter.default.addObserver(self, selector: #selector(changeLang(_ : )), name: Notification.Name("changeLang"), object: nil)
    }
    
    @objc func changeLang(_ notification: Notification){
        if let lang = notification.userInfo?["lang"] as? Bool{
            if lang{
                AppService.shared.changeLang(lang: "en")
                self.title = "Articles".localizedString()
                self.searchController.searchBar.placeholder = "Search".localizedString()
           }else{
               AppService.shared.changeLang(lang: "km")
               self.title = "Articles".localizedString()
               self.searchController.searchBar.placeholder = "Search".localizedString()
           }
        }
    }
    
    func fetch(){
        ProgressHUD.show()
        Network.shared.fetchArticles { result in
            switch result{
            case .success(let articles):
                self.articles = articles
                self.filteredArticle = articles
                self.tableVIew.reloadData()
                ProgressHUD.showSucceed("Get data successfully")
                self.tableVIew.refreshControl?.endRefreshing()
                
            case .failure(let error):
                ProgressHUD.showError(error.localizedDescription)
                self.tableVIew.refreshControl?.endRefreshing()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailArticle" {
            if let desVC = segue.destination as? DetailtViewController, let indexPath = sender as? IndexPath{

                let article = self.articles[indexPath.row]
                
                desVC.article = article
            }
           
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.articles.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "detailArticle", sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellArticle", for: indexPath) as! ArticleTableViewCell
        cell.config(article: self.articles[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .destructive, title: "Delete"){ (action,view, completion) in
            
            let alert = UIAlertController(title: "Are you sure to delete?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                let id = self.articles[indexPath.row].id
                Network.shared.deleteArticles(id)
                self.articles.remove(at: indexPath.row)
                self.tableVIew.deleteRows(at: [indexPath], with: .fade)
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            completion(true)
        }
        
        let edit = UIContextualAction(style: .normal, title: "Edit"){ (action, view, completion) in
            
            let updateVC = self.storyboard?.instantiateViewController(withIdentifier: "editScreen") as! PostViewController
            
            let article = self.articles[indexPath.row]
            
            updateVC.updateArticle = article
            updateVC.isUpdate = true
            
            self.navigationController?.pushViewController(updateVC, animated: true)
            
        }
        
        let config = UISwipeActionsConfiguration(actions: [delete, edit])
              config.performsFirstActionWithFullSwipe = false
        
        return config
    }
}

extension ViewController: UISearchControllerDelegate, UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        search(searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        search("")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let query = searchBar.text, !query.isEmpty else { return }
    }
    
    func search(_ query: String) {
        if query.count >= 1 {
            self.articles = self.filteredArticle.filter { $0.title.lowercased().contains(query.lowercased()) }
        } else{
            self.articles = self.filteredArticle
        }

        tableVIew.reloadData()
    }
}
