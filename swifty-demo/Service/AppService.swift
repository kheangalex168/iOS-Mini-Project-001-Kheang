//
//  AppService.swift
//  swifty-demo
//
//  Created by Kheang on 27/11/21.
//

import Foundation

struct AppService{
    static let shared = AppService()
    
    var language: String {
        return UserDefaults.standard.string(forKey: "lang") ?? "en"
    }
    
    func changeLang(lang: String){
        UserDefaults.standard.set(lang, forKey: "lang")
    }
}
